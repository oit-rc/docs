# Version_control

## Why use version control?
Version control is a system that records changes to a file or set of files over time so that you can recall specific versions later. For the examples below, we will be using the distributed version control system Git.Version control systems provide several benefits, including:

- Collaboration: Multiple developers can work on the same project simultaneously, and version control allows for efficient collaboration and tracking of changes made by each developer.

- Backup and Recovery: Version control systems provide a history of changes made to the codebase, allowing for easy rollback in case of bugs or issues.

- Tracking Changes: Version control systems keep track of all changes made to the codebase, who made them, and when they were made.

- Branching and Merging: Git allows for the creation of multiple branches of a project, allowing for parallel development and easy merging of changes into a single codebase.

- Code Review: Changes made by developers can be easily reviewed and commented on before they are merged into the main codebase.  


Overall, version control systems help to improve the development process, ensure code quality, and facilitate teamwork and collaboration.

## Introduction
For version control, a popular choice is Git, which is a distributed version control system.

GitHub is a web-based platform for version control and collaboration that uses Git. You can find the documentation for [GitHub] (https://docs.github.com/)

GitLab is another web-based platform for Git repository management and version control. You can find the documentation for [GitLab] (here: https://docs.gitlab.com/)

Another option is Bitbucket, which is a web-based platform for version control and collaboration that uses Git and Mercurial. You can find the documentation for [Bitbucket](https://bitbucket.org/product/guides )

Additionally, you can find the official documentation for Git itself [here] (https://git-scm.com/doc)