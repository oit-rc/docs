# Connecting to GitLab/GitHub
## When to Use HTTPS and When to Use SSH

HTTPS and SSH are both protocols used to connect to Git repositories, but they have some key differences.

HTTPS:

Uses standard ports (443)
Does not require the use of SSH keys
Uses your GitLab account credentials for authentication
SSH:

Uses a specific port (22)
Requires the use of SSH keys for authentication
More secure than HTTPS  

**HTTPS is generally easier to set up and use, but SSH is more secure. If security is a concern, it is recommended to use SSH. Otherwise, HTTPS is a good choice for its ease of use.**

In addition, SSH is more suitable for automation, because it does not require user interaction.

## Connecting to GitLab Using HTTPS

Open a web browser and navigate to the GitLab repository you want to connect to.

Click on the "Clone" button in the top right corner of the repository page.

In the "Clone with HTTPS" section, copy the URL by clicking the "Copy" button.

Open a terminal or command prompt and navigate to the directory where you want to clone the repository.

Run the following command to clone the repository:
```
git clone [copied URL]
``` 
If prompted, enter your GitLab username and password to authenticate the clone.

Once the repository is cloned, you can make changes, commit, and push as normal.

## Generating SSH Keys on Windows
Open the Git Bash terminal (installed with Git for Windows)
Type ssh-keygen and press enter
Enter a file in which to save the key (e.g. C:\Users\username\.ssh\id_rsa)
Press enter to use the default file location
Enter a passphrase (or leave it blank for no passphrase) and press enter
You should now have a private key (id_rsa) and a public key (id_rsa.pub) in the specified directory
## Generating SSH Keys on Linux
Open the terminal
Type ssh-keygen and press enter
Press enter to use the default file location
Enter a passphrase (or leave it blank for no passphrase) and press enter
You should now have a private key (id_rsa) and a public key (id_rsa.pub) in the default ~/.ssh directory
## Adding an SSH Key to GitLab
Go to your GitLab profile settings
Select "SSH Keys" from the left sidebar
Click the "Add SSH Key" button
Give your key a title (e.g. "My work laptop")
Open the id_rsa.pub file in a text editor and copy the contents
Paste the contents into the "Key" field in GitLab
Click the "Add key" button
You should now be able to use SSH to access GitLab
Note: if you encounter any problem with ssh-keygen, you may want to run ssh-keygen -t rsa -b 4096



