![(duke)](https://sicss.io/assets/images/duke.jpg)
# Home

Welcome to the Duke Compute Cluster(DCC) user documentation site. You'll find overviews, documentation, FAQs, and tips on the Duke Compute Cluster on these pages.

The Duke Compute Cluster is a general purpose high performance/high-throughput installation, and it is fitted with software used for a broad array of scientific projects. With a few notable exceptions, applications on the cluster are generally Free and Open Source Software. 

Quick facts:

* 1300 nodes which combined have more than 30,000 vCPUs, 750 GPUs and 200TB of RAM
* Interconnects are 10 Gbps or 40 Gbps
* Utilizes a 7 Petabyte Isilon file system
* Runs CentOS 8 Stream and SLURM is the job scheduler

The DCC is managed and supported by the [Office of Information Technology (OIT)](https://oit.duke.edu) and [Research Computing](https://rc.duke.edu). 
