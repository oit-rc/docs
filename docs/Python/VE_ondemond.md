## Setting up a new virtual environment in JupyterLab Terminal
1. Press 'Connect to Jupyter' on OnDemand and navigate to the 'Terminal' under Other from the launcher.
2. Create a new virtual environment with the desired Python version by running the following command: 
example: test is the name of the environment and python=3.9 is the version of python
```
conda create -n test python=3.9
```
3. Activate the new environment by running the command: 
```
conda activate test
```
4. Install the ipykernel package by running the command: 
```
conda install ipykernel
```
4. Register the new environment as a Jupyter kernel by running the command: 
```
python -m ipykernel install --user --name test
```
5. You should now see the new environment "test" as an option on the Launcher when creating a new notebook 
6. To switch to this new environment on a running notebook:
 - Go to Kernel menu on the left to 'change kernel' and select the environment.
 - click on the present kernel name on the top right corner and select the environment.
7. To deactivate the environment, you can use conda deactivate command
**Note: You can use conda or pip to install packages in this virtual environment. If you want to use the virtual environment outside jupyter notebook, you can use the command prompt or terminal and activate it by using conda activate test**