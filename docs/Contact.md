## Key Contacts

## Location

### Access to Systems

### General Inquiries/Events

### Website Inquiries

### Research Software Engineering

### Visualization

### Training and Workshops

### Resources, Services and Support

## Subscribe

**[Subscribe](?)** to Research Computing events email list.

## Mailing Address

### OIT Research Computing

300 Fuller St, Durham, NC 27701


**Campus Map:** [(oit)](https://www.google.com/maps/place/Duke+Office+of+Information+Technology/@36.0005074,-78.9055498,15z/data=!4m5!3m4!1s0x0:0xabbbfc2762e188be!8m2!3d36.0006199!4d-78.9057298)