1.	Create a subdirectory within your HPC group directory called "rlib" to store your installed and compiled packages.
2.	Use the ```install.packages()``` function with the repos and lib parameters to specify the source repository and target installation directory, respectively. This will ensure that dependent packages are also installed.
3.	Set the R library path using ```.libPaths()``` to include the target installation directory, so R can locate and load the installed packages and their dependencies.
4.	If you need to install a package from a local file, download the package source file and install it using the ```install.packages() ```function.
5.	In a Slurm batch, use the Rscript command to execute R scripts and load a version of R. Prior to execution, install any packages required in your R script using an interactive R session and store them in a directory dedicated to the specific R version being used.

