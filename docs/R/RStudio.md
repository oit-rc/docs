# Manage packages in RStudio

## Use the Console 
Open RStudio and ensure that the "Console" pane is active.

```
install.packages("package_name") # to install a package
```
```
update.packages() # to update all installed packages
```
```
remove.packages("package_name") # to remove a package

```

If you want to install multiple packages at once, you can use the c() function to create a vector of package names, and pass that vector to the install.packages() function. For example, to install the "ggplot2" and "dplyr" packages, you could run the following command:

```
install.packages(c("ggplot2", "dplyr"))
```
Once the installation is complete, you can load the package into your R session using the library() function. For example, to load the "ggplot2" package, you would run the following command:

```
library(ggplot2)
```
Note: If you are behind a proxy, you will need to configure your proxy settings in R before you can install packages. This can be done by running the following command:

```
Sys.setenv(http_proxy="http://proxy_host:proxy_port/")
```

## Use the Packages pane
- Click on the "Packages" tab in the lower right panel
- Use the "Install" button to install new packages
- Use the "Update" button to update packages that are already installed
- Use the "Uninstall" button to remove packages that are no longer needed
![Screen Shot 2023-02-01 at 12 09 10 PM](https://user-images.githubusercontent.com/90811429/216114076-e0db982e-904f-490e-850c-a82097178ff8.png)

Note: Always check the dependencies before uninstalling a package, as uninstalling a package may result in other packages that depend on it to stop working.

**For more information, see the [RStudio documentation](https://support.rstudio.com/hc/en-us/articles/200486498-Package-Management).**